#!/usr/bin/env python3
import SquareCrosswordGenerator as square_gen
import json
from re import search as f
from termcolor import colored as c
import sys

def export_to_json(square_crossword):
    json_dictionary = {}
    json_dictionary['rows'] = [[len(regex.text_sequence), regex.regex_sequence] for regex in square_crossword['rows_regexes']]
    json_dictionary['cols'] = [[len(regex.text_sequence), regex.regex_sequence] for regex in square_crossword['cols_regexes']]
    return(json_dictionary)

def simple_square(pattern):
    pattern_rows_1 = pattern['rows'][0][1]
    pattern_cols_1 = pattern['cols'][0][1]
    pattern_rows_2 = pattern['rows'][1][1]
    pattern_cols_2 = pattern['cols'][1][1]
    max_cell_len = len(pattern_cols_1)+2 if len(pattern_cols_1) > len(pattern_cols_2) else len(pattern_cols_2)
    max_left_indent = len(pattern_rows_1) if len(pattern_rows_1) > len(pattern_rows_2) else len(pattern_rows_2)
    additive = max_left_indent - (len(pattern_rows_1) if len(pattern_rows_1)<len(pattern_rows_2) else len(pattern_rows_2))
    path2 = additive+2 if len(pattern_rows_2) > len(pattern_rows_1) else 0
    path3 = additive+2 if len(pattern_rows_1) > len(pattern_rows_2) else 0

    print("""
{0}{1}{5}{9}{6}
{0}{8}{3}{8}{3}{8}
{2}{8}{4}{8}{4}{8}
{0}{8}{3}{8}{3}{8}
{7}{8}{4}{8}{4}{8}
{0}{8}{3}{8}{3}{8}
          """.format(''.rjust(max_left_indent, ' '), \
          pattern_cols_1, \
          pattern_rows_1.rjust(path2), \
          c((''.center(max_cell_len, '#')),'green'), \
          ''.rjust(max_cell_len), \
          ''.ljust(max_cell_len-len(pattern_cols_1)+2), \
          pattern_cols_2, pattern_rows_2.rjust(path3), c('#', 'green'), c("|", 'green')))
    return[pattern_rows_1, pattern_rows_2, pattern_cols_1, pattern_cols_2]

def _simple(count):
        mass = simple_square((export_to_json(square_gen.random_square_crossword(2))))
        answer = sys.stdin.readline().strip().rstrip().split()
        try:
                pr1 = mass[0]
                pc1 = mass[2]
                pr2 = mass[1]
                pc2 = mass[3]
                user_pr1 = answer[0] + answer[1]
                user_pr2 = answer[2] + answer[3]
                user_pc1 = answer[0] + answer[2]
                user_pc2 = answer[1] + answer[3]
        except:
                print('Not all fields are filled')
                return

        if f(pc1, user_pc1) and f(pc2, user_pc2) and f(pr2, user_pr2) and f(pr1, user_pr1):
                count += 1
        else:
                count = False
        return(count)

print(c("Sir, are you human? Please, type verification!", 'red'))
print(c("""\n\nThis is example:
________________________________________________
INPUT:

             l6   Sp|sI|An
        ###################
FU|ls|hJ#  0     #     1  #
        ###################
[6wIB]+ #     2  #     3  #
        ###################
________________________________________________
""", 'blue'))

print(c("""
________________________________________________
SOLUTION:
0 = matches (l6) and (FU|ls|hJ) = l
1 = matches (FU|ls|hJ) and FU|ls|hJ = s
2 = matches ([6wIB]+) and l6 = 6
3 = matches (Sp|sI|An) and ([6wIB]+) = I

How it's checking:
if re.search(answer[0]+answer[2], "l6") and re.search(answer[0]+answer[1], "FU|ls|hJ") and re.search(answer[1]+answer[3], "Sp|sI|An") and re.search(answer[2]+answer[3], "[6wIB]+"):
        return("ok")
________________________________________________
OUTPUT:
l s 6 I
________________________________________________
\n\n
""", 'green'))

count = 0
while count < 201:
        try:
                count = _simple(count)
                if count:
                        print(c('\n\n[c]+o{2}[l]+', 'blue'))
                        print(c("You score: {}\200".format(count), 'green'))
                        if count == 5:
                                print("LetoCTF{1_\1k3_r3G3x}")
                                sys.exit(1)
                else: 
                        print(c("HUMAN DETECTED", 'red'))
                        count = 0
                        sys.exit(1)
        except:
                continue
