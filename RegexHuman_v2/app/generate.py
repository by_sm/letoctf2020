#!/usr/bin/env python3
import SquareCrosswordGenerator as square_gen
import json
from re import search as f
from termcolor import colored as c
import sys
from random import random

def export_to_json(square_crossword):
    json_dictionary = {}
    json_dictionary['rows'] = [[len(regex.text_sequence), regex.regex_sequence] for regex in square_crossword['rows_regexes']]
    json_dictionary['cols'] = [[len(regex.text_sequence), regex.regex_sequence] for regex in square_crossword['cols_regexes']]
    return(json_dictionary)

def simple_square(pattern):
    pattern_rows_1 = pattern['rows'][0][1]
    pattern_cols_1 = pattern['cols'][0][1]
    pattern_rows_2 = pattern['rows'][1][1]
    pattern_cols_2 = pattern['cols'][1][1]
    max_cell_len = len(pattern_cols_1)+2 if len(pattern_cols_1) > len(pattern_cols_2) else len(pattern_cols_2)
    max_left_indent = len(pattern_rows_1) if len(pattern_rows_1) > len(pattern_rows_2) else len(pattern_rows_2)
    additive = max_left_indent - (len(pattern_rows_1) if len(pattern_rows_1)<len(pattern_rows_2) else len(pattern_rows_2))
    path2 = additive+2 if len(pattern_rows_2) > len(pattern_rows_1) else 0
    path3 = additive+2 if len(pattern_rows_1) > len(pattern_rows_2) else 0

    print("""
{0}{1}{5}{6}
{0}{8}{3}{8}{3}{8}
{2}{8}{4}{8}{4}{8}
{0}{8}{3}{8}{3}{8}
{7}{8}{4}{8}{4}{8}
{0}{8}{3}{8}{3}{8}
          """.format(''.rjust(max_left_indent), \
          pattern_cols_1, \
          pattern_rows_1.rjust(path2), \
          c(''.center(max_cell_len, '#'), 'green'), \
          ''.rjust(max_cell_len), \
          ''.ljust(max_cell_len-len(pattern_cols_1)+2), \
          pattern_cols_2, pattern_rows_2.rjust(path3),\
         c("#", 'green')))
    return[pattern_rows_1, pattern_rows_2, pattern_cols_1, pattern_cols_2]

def square4(pattern):
        pr1 = pattern['rows'][0][1]
        pc1 = pattern['cols'][0][1]
        pr2 = pattern['rows'][1][1]
        pc2 = pattern['cols'][1][1]
        pr3 = pattern['rows'][2][1]
        pc3 = pattern['cols'][2][1]
        pr4 = pattern['rows'][3][1]
        pc4 = pattern['cols'][3][1]

        tmp = [len(pr1), len(pr2), len(pr3), len(pr4)]
        max_left_indent = (max(tmp))
        tmp = [len(pc1), len(pc2), len(pc3), len(pc4)]
        max_cell_len = (max(tmp))
        
        print("""
{0}{11}{7}{11}{8}{11}{9}{11}{10}{11}
{0}{12}{1}{12}{1}{12}{1}{12}{1}{12}
{3}{12}{2}{12}{2}{12}{2}{12}{2}{12}
{0}{12}{1}{12}{1}{12}{1}{12}{1}{12}
{4}{12}{2}{12}{2}{12}{2}{12}{2}{12}
{0}{12}{1}{12}{1}{12}{1}{12}{1}{12}
{5}{12}{2}{12}{2}{12}{2}{12}{2}{12}
{0}{12}{1}{12}{1}{12}{1}{12}{1}{12}
{6}{12}{2}{12}{2}{12}{2}{12}{2}{12}
{0}{12}{1}{12}{1}{12}{1}{12}{1}{12}

""".format(''.rjust(max_left_indent),\
           c(''.center(max_cell_len, '#'), 'green'), \
           ''.rjust(max_cell_len), \
            pr1.rjust(max_left_indent), pr2.rjust(max_left_indent), \
            pr3.rjust(max_left_indent), pr4.rjust(max_left_indent), \
                    pc1.center(max_cell_len), pc2.center(max_cell_len), pc3.center(max_cell_len), pc4.center(max_cell_len),\
                            c("|", 'green'), c('#', 'green')))
        return[pr1, pr2, pr3, pr4, pc1, pc2, pc3, pc4]


def square3(pattern):
        pr1 = pattern['rows'][0][1]
        pc1 = pattern['cols'][0][1]
        pr2 = pattern['rows'][1][1]
        pc2 = pattern['cols'][1][1]
        pr3 = pattern['rows'][2][1]
        pc3 = pattern['cols'][2][1]
 
        tmp = [len(pr1), len(pr2), len(pr3)]
        max_left_indent = (max(tmp))
        tmp = [len(pc1), len(pc2), len(pc3)]
        max_cell_len = (max(tmp))
        
        print("""
{0}{9}{6}{9}{7}{9}{8}{9}
{0}{10}{1}{10}{1}{10}{1}{10}
{3}{10}{2}{10}{2}{10}{2}{10}
{0}{10}{1}{10}{1}{10}{1}{10}
{4}{10}{2}{10}{2}{10}{2}{10}
{0}{10}{1}{10}{1}{10}{1}{10}
{5}{10}{2}{10}{2}{10}{2}{10}
{0}{10}{1}{10}{1}{10}{1}{10}
 
""".format(''.rjust(max_left_indent),\
           c(''.center(max_cell_len, '#'), 'green'), \
           ''.rjust(max_cell_len), \
            pr1.rjust(max_left_indent), pr2.rjust(max_left_indent), \
            pr3.rjust(max_left_indent), pc1.center(max_cell_len), \
            pc2.center(max_cell_len), pc3.center(max_cell_len), c("|", 'green'), c('#', 'green')))
        return(pr1, pr2, pr3, pc1, pc2, pc3)

def _square_4(count):
        mass = square4(export_to_json(square_gen.random_square_crossword(4)))
        answer = sys.stdin.readline().strip().rstrip().split()
        try:
                pr1 = mass[0]
                pc1 = mass[4]
                pr2 = mass[1]
                pc2 = mass[5]
                pr3 = mass[2]
                pc3 = mass[6]
                pr4 = mass[3]
                pc4 = mass[7]

                user_pr1 = answer[0] + answer[1] + answer[2] + answer[3]
                user_pr2 = answer[4] + answer[5] + answer[6] + answer[7]
                user_pr3 = answer[8] + answer[9] + answer[10] + answer[11]
                user_pr4 = answer [12] + answer[13] + answer[14] + answer[15]
                user_pc1 = answer[0] + answer[4] + answer[8] + answer[12]
                user_pc2 = answer[1] + answer[5] + answer[9] + answer[13]
                user_pc3 = answer[2] + answer[6] + answer[10] + answer[14]
                user_pc4 = answer[3] + answer[7] + answer[11] + answer[15]
        except:
                print('Not all fields are filled ')
                return
        #very big if,lol
        if f(pc1, user_pc1) and f(pc2, user_pc2) and f(pc3, user_pc3) and f(pc4, user_pc4) and f(pr1, user_pr1) and f(pr2, user_pr2) and f(pr3, user_pr3) and f(pr4, user_pr4):
                count += 1
        else:
                count = False
        return(count)

def _simple(count):
        mass = simple_square((export_to_json(square_gen.random_square_crossword(2))))
        answer = sys.stdin.readline().strip().rstrip().split()
        try:
                pr1 = mass[0]
                pc1 = mass[2]
                pr2 = mass[1]
                pc2 = mass[3]
                user_pr1 = answer[0] + answer[1]
                user_pr2 = answer[2] + answer[3]
                user_pc1 = answer[0] + answer[2]
                user_pc2 = answer[1] + answer[3]
        except:
                print('Not all fields are filled')
                return

        if f(pc1, user_pc1) and f(pc2, user_pc2) and f(pr2, user_pr2) and f(pr1, user_pr1):
                count += 1
        else:
                count = False
        return(count)

def _square_3(count):
        mass = square3(export_to_json(square_gen.random_square_crossword(3)))
        answer = sys.stdin.readline().strip().rstrip().split()
        try:
                pr1 = mass[0]
                pc1 = mass[3]
                pr2 = mass[1]
                pc2 = mass[4]
                pr3 = mass[2]
                pc3 = mass[5]

                user_pr1 = answer[0] + answer[1] + answer[2]
                user_pr2 = answer[3] + answer[4] + answer[5]
                user_pr3 = answer[6] + answer[7] + answer[8]
                user_pc1 = answer[0] + answer[3] + answer[6]
                user_pc2 = answer[1] + answer[4] + answer[7]
                user_pc3 = answer[2] + answer[5] + answer[8]
        except:
                print('Not all fields are filled ')
                return
        #very big if,lol
        if f(pr1, user_pr1) and f(pr2, user_pr2) and f(pr3, user_pr3) and f(pc1, user_pc1) and f(pc2, user_pc2) and f(pc3, user_pc3):
                count += 1
        else:
                count = False
        return(count)

print(c("Security verification l2, HUMAN NOT PASS!!!", 'red'))

count = 0
while count < 251:
        try:
                t = random()
                if t <= 0.3:
                        count = _simple(count)
                elif t > 0.3 and t <= 0.6:
                        count = _square_4(count)
                else:
                        count = _square_3(count)
                if count:
                        print(c('\n\n[c]+o{2}[l]+', 'blue'))
                        print(c("You score: {}\250".format(count), 'green'))
                        if count == 250:
                                print("LetoCTF{2393x_15_my_11f3}")
                                sys.exit(1)
                else: 
                        print(c("HUMAN DETECTED", 'red'))
                        count = 0
                        sys.exit(1)
        except:
                continue

